; test-file for bin-tree
(load "bin-tree.scm")

; test of insert and delete
(define tree1 (make-node '() 25 '()))
(define tree2 (insert-element (insert-element (insert-element (insert-element (insert-element (insert-element (insert-element tree1 13) 30) 28) 60) 7) 20) 17))

; test of output functions
(inorder-output tree2)
(newline)
(preorder-output tree2)
(newline)
(postorder-output tree2)
;(inorder-output (delete-element tree2 17))