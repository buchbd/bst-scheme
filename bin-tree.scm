; make a node
(define (make-node left-branch element right-branch)
  (cons left-branch (cons element right-branch)))

; return left-branch
(define (get-left-branch node)
  (car node))

; return right-branch
(define (get-right-branch node)
  (cddr node))

; return root
(define (get-root node)
  (cadr node))

; return if node is leaf
(define (is-leaf? node)
  (and (null? (get-left-branch node)) (null? (get-right-branch node))))

; returns if left child is null
(define (has-left-child? node)
  (not (null? (get-left-branch node))))

; returns if right child is null
(define (has-right-child? node)
  (not (null? (get-right-branch node))))

; returns maximum of tree
(define (get-max tree)
  (if (has-right-child? tree)
      (get-max (get-right-branch tree))
      (get-root tree)))

;returns minimum of tree
(define (get-min tree)
  (if (has-left-child? tree)
      (get-min (get-left-branch tree))
      (get-root tree)))

; insert-element in tree
(define (insert-element tree element)
  (cond
    ((< element (get-root tree))
     (if (has-left-child? tree)
         (make-node (insert-element (get-left-branch tree) element) (get-root tree) (get-right-branch tree))
         (make-node (make-node '() element '()) (get-root tree) (get-right-branch tree))))
    ((> element (get-root tree))
     (if (has-right-child? tree)
         (make-node (get-left-branch tree) (get-root tree) (insert-element (get-right-branch tree) element))
         (make-node (get-left-branch tree) (get-root tree) (make-node '() element '()))))))

; delete element from tree
(define (delete-element tree element)
  (cond
    ((= element (get-root tree))
     (cond
       ((is-leaf? tree) '())
       ((not (has-right-child? tree)) (get-left-branch tree))
       ((not (has-left-child? tree)) (get-right-branch tree))
       (else
        (make-node (get-left-branch tree) (get-min (get-right-branch tree)) (delete-element (get-right-branch tree) (get-min (get-right-branch tree)))))))
    ((< element (get-root tree))
     (make-node (delete-element (get-left-branch tree) element) (get-root tree) (get-right-branch tree)))
    (else
     (make-node (get-left-branch tree) (get-root tree) (delete-element (get-right-branch tree) element)))))
  
; inorder-output
(define (inorder-output tree)
  (begin
    (if (has-left-child? tree)
        (inorder-output (get-left-branch tree)))
    (display (get-root tree))
    (newline)
    (if (has-right-child? tree)
        (inorder-output (get-right-branch tree)))))

; preorder-output
(define (preorder-output tree)
  (begin
    (display (get-root tree))
    (newline)
    (if (has-left-child? tree)
        (preorder-output (get-left-branch tree)))
    (if (has-right-child? tree)
        (preorder-output (get-right-branch tree)))))

; postorder-output
(define (postorder-output tree)
  (begin
    (if (has-left-child? tree)
        (postorder-output (get-left-branch tree)))
    (if (has-right-child? tree)
        (postorder-output (get-right-branch tree)))
    (display (get-root tree))
    (newline)))